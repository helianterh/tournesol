﻿using System.Collections.Specialized;

namespace Tournesol.Vault.Client
{
    public static class VaultClient
    {
        public static HttpClient _client = new HttpClient()
        {
            BaseAddress = new Uri("https://localhost:7242")
        };

        public static async Task<string> GetSecretAsync(Guid serviceId)
        {
            UriBuilder builder = new UriBuilder(_client.BaseAddress + "api/v1/GetSecret");
            NameValueCollection collection = System.Web.HttpUtility.ParseQueryString(string.Empty);
            collection.Add("serviceId", serviceId.ToString());
            builder.Query = collection.ToString();

            string response = await _client.GetStringAsync(builder.Uri);
            return response;
        }
    }
}