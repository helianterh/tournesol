﻿using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.Model
{
    public static class ContentServices
    {
        private static Dictionary<Guid, IContentService> _cache = _InitCache();

        public static IContentService GetContentService(Guid guid)
        {
            return _cache[guid];
        }

        private static Dictionary<Guid, IContentService> _InitCache()
        {
            Dictionary<Guid, IContentService> dictio = new Dictionary<Guid, IContentService>();

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(IContentService).IsAssignableFrom(p));

            foreach(var type in types)
            {
                var contentService = Activator.CreateInstance(type) as IContentService;
                if(contentService != null && dictio.ContainsKey(contentService.ServiceId))
                    dictio.Add(contentService.ServiceId, contentService);
            }

            return dictio;
        }
    }
}
