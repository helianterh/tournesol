﻿using Tournesol.Contents.Common.Interface;

namespace Tournesol.Content
{
    public class FacadeUser : IFacadeUser
    {
        private static Guid _guid = Guid.NewGuid();

        public Guid GetCurrentUserGuid()
        {
            return _guid;
        }
    }
}
