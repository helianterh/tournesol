using Tournesol.Contents.Persistence;
using Microsoft.EntityFrameworkCore;
using Tournesol.Contents.Common;
using Tournesol.Content;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;
using Tournesol.Vault.Client;
using Tournesol.Extension;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers().AddJsonOptions(opt => opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
builder.Services.AddSwaggerGen(c => {
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 1safsfsdfdfd\"",
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement {
        {
            new OpenApiSecurityScheme {
                Reference = new OpenApiReference {
                    Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                }
            },
            new string[] {}
        }
    });
});



builder.Services.AddControllers();

builder.Services.AddScoped(provider => 
    new ContentRepository(
        new ContentFacadePersistanceEFCore(b =>
        {
            b.UseSqlite("DataSource=Content.db");
        },
        (c,firstTime) =>
        {
            if (firstTime)
            {
                //c.Database.EnsureDeleted();
                c.Database.EnsureCreated();
            }
        }),
        new FacadeUser()
    )
);

var autentification = builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
});

foreach (var service in Services.GetServices())
    autentification.AddJwtBearer(service.Name, o =>
    {
        o.TokenValidationParameters = new TokenValidationParameters
        {
            ValidIssuer = service.ValidIssuer,
            ValidAudience = service.ValidAudience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(VaultClient.GetSecretAsync(service.ServiceId).GetAwaiter().GetResult())),
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = false,
            ValidateIssuerSigningKey = true
        };
    });

var app = builder.Build();

app.UseCors(builder => builder.WithOrigins("https://localhost:44475")
                                .AllowAnyMethod()
                                .AllowAnyHeader()
                                .AllowAnyOrigin());

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers().RequireAuthorization();

app.Run();
