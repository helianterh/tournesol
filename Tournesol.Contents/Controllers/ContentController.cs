﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Specialized;
using Tournesol.Contents.Common;
using Tournesol.Contents.Common.Model;
using Tournesol.Contents.Model;
using Tournesol.Extension;

namespace Tournesol.Contents.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = Services.ContentAuthenticationSchemes)]
    public class ContentController : ControllerBase
    {

        private ContentRepository _contentRepository;
        public ContentController(ContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        [Route("/api/v1/GetContents")]
        [HttpGet]
        public IEnumerable<Common.Model.Content> GetContents(Guid serviceId, int contentTypeId)
        {
            return _contentRepository.GetContents(serviceId, contentTypeId);
        }

        [Route("/api/v1/GetContent")]
        [HttpGet]
        public Common.Model.Content? GetContent(Guid contentId)
        {
            return _contentRepository.GetContent(contentId);
        }


        public class ContentParameters
        {
            public Guid serviceId { get; set; }
            public int contentTypeId { get; set; }

            public Guid? parentId { get; set; }

            public string json { get; set; }
        }

        [Route("/api/v1/AddContent")]
        [HttpPost]
        public Common.Model.Content AddContent(ContentParameters parameters)
        {
            return _contentRepository.AddContent(ContentServices.GetContentService(parameters.serviceId), parameters.contentTypeId, parameters.parentId, parameters.json);
        }

        [Route("/api/v1/DeleteContent")]
        [HttpPost]
        public void DeleteContent(Guid contentId)
        {
            _contentRepository.DeleteContent(contentId);
        }

        [Route("/api/v1/UpdateContent")]
        [HttpPost]
        public Common.Model.Content UpdateContent(Guid contentId, string json)
        {
            return _contentRepository.UpdateContent(contentId, json);
        }

        [Route("/api/v1/PublishContent")]
        [HttpPost]
        public void PublishContent(Guid contentId)
        {
            _contentRepository.PublishContent(contentId);
        }


    }
}