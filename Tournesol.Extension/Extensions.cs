﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static Tournesol.Attributes.Attributes;

namespace Tournesol.Extension
{
    public static class Extensions
    {
        public static string ToJson(this object obj)
        {
            var settings = new JsonSerializerSettings();
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            return JsonConvert.SerializeObject(obj, settings);
        }

        public static IEnumerable<T> Yield<T>(this T obj)
        {
            yield return obj;
        }

        public static T GetAttribute<T>(this Enum value) where T : Attribute
        {
            var type = value.GetType();
            var memberInfo = type.GetMember(value.ToString());
            var attributes = memberInfo[0].GetCustomAttributes(typeof(T), false);
            return attributes.Length > 0
              ? (T)attributes[0]
              : null;
        }
       
        public static string KeyValue(this Enum value)
        {
            var attribute = value.GetAttribute<KeyValueAttribute>();
            return attribute == null ? value.ToString() : attribute.KeyValue;
        }

        public static int Id(this Enum value)
        {
            return Convert.ToInt32(value);
        }



    }
}