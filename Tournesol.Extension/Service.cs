﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Extension
{
    public class Service
    {
        public Guid ServiceId { get; set; }
        public string Name { get; set; }

        public string ValidIssuer { get { return $"Tournesol.{Name}.Issuer"; } }

        public string ValidAudience { get { return $"Tournesol.{Name}.Audience"; } }


        public Service(Guid serviceId, string name)
        {
            ServiceId = serviceId;
            Name = name;
        }
    }
}
