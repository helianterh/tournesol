﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Extension
{
    public static class Services
    {
        public static Service BLOG = new Service(Guid.Parse("E0E0A554-E3A0-4CDA-B2B0-9B6519B5D1C6"), "Blog");
        public static Service REPORT = new Service(Guid.Parse("DB8CC657-5A20-4303-8E7B-E00CC3BB9BC6"), "Report");
        public const string ContentAuthenticationSchemes = "Blog,Report";

        private static Dictionary<Guid, Service> _cache = _InitCache();

        public static Service GetService(Guid serviceId)
        {
            return _cache[serviceId];
        }

        public static IEnumerable<Service> GetServices()
        {
            return _cache.Values;
        }

        private static Dictionary<Guid, Service> _InitCache()
        {
            return new Dictionary<Guid, Service>()
            {
                { BLOG.ServiceId, BLOG  },
                { REPORT.ServiceId, REPORT  },
            };
        }
    }
}
