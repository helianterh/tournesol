﻿using Tournesol.Extension;
using static Tournesol.Attributes.Attributes;

namespace Tournesol.Contents.Common.Model
{
    public enum VersionState
    {
        [KeyValue("ADD")]
        Added = 0,

        [KeyValue("PUB")]
        Published = 1,

        [KeyValue("DEL")]
        Deleted = -1,

        [KeyValue("BAN")]
        Banned = -2,
    }

    public static class Enumerations
    {
        private static Dictionary<Tuple<Type, string>, Enum> _cache = InitCache();

        public static T Get<T>(string keyValue) where T : Enum
        {
            var key = Tuple.Create(typeof(T), keyValue);
            if (_cache.ContainsKey(key))
                return (T)_cache[key];

            throw new TournesolException("KeyValue is not corresponding to an existing Enum.");
        }

        private static Dictionary<Tuple<Type, string>, Enum> InitCache()
        {
            var dictio = new Dictionary<Tuple<Type, string>, Enum>();

            Action<Type> addCache = type =>
            {
                foreach (Enum enumeration in Enum.GetValues(type))
                    dictio.Add(Tuple.Create(type, enumeration.KeyValue()), enumeration);
            };

            addCache(typeof(VersionState));
            return dictio;
        }
    }
}
