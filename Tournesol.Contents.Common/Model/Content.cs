﻿using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.Common.Model
{
    public class Content
    {
        public Guid? ContentId { get; set; }

        public Guid ServiceId { get; set; }

        public int ContentType { get; set; }

        public Guid? ParentId { get; set; }

        public string Json { get; set; }

        public string Api { get; set; }

        public VersionState State { get; set; }

        public Guid UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Content? Parent { get; set; }

        public ICollection<Content>? Children { get; set; }

        public ICollection<ContentVersion>? Versions { get; set; }

        public Content(Guid? contentId, Guid serviceId, Guid? parentId, int contentType, string json, string api, VersionState state, Guid updatedBy, DateTime updatedDate, Guid createdBy, DateTime createdDate)
        {
            ContentId = contentId;
            ServiceId = serviceId;
            ParentId = parentId;
            ContentType = contentType;
            Json = json;
            Api = api;
            State = state;
            UpdatedBy = updatedBy;
            UpdatedDate = updatedDate;
            CreatedBy = createdBy;
            CreatedDate = createdDate;
        }
   
        public T GetContentObject<T>() where T : IContentObject
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(this.Json);
        }
    }
}
