﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Model
{
    public class ContentVersion
    {
        public Guid? ContentId { get; set; }

        public Guid? VersionId { get; set; }

        public string Json { get; set; }

        public string Api { get; set; }

        public VersionState State { get; set; }

        public Guid UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public Content? Content { get; set; }

        public ContentVersion(Guid? contentId, Guid? versionId, string json, string api, VersionState state, Guid updatedBy, DateTime updatedDate)
        {
            ContentId = contentId;
            VersionId = versionId;
            Json = json;
            Api = api;
            State = state;
            UpdatedBy = updatedBy;
            UpdatedDate = updatedDate;
        }

        internal ContentVersion(Content content)
        {
            ContentId = content.ContentId;
            Json = content.Json;
            Api = content.Api;
            State = content.State;
            UpdatedBy = content.UpdatedBy;
            UpdatedDate = content.UpdatedDate;
        }
    }
}
