﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Blug.Common.Model
{
    public interface ILog
    {
        Guid? LogId { get; set; }

        Guid LogBy { get; set; }

        DateTime LogDate { get; set; }
    }
}
