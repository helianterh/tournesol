﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Interface
{
    public abstract class ContentService<T> : IContentService where T : Enum
    {
        public abstract Guid ServiceId { get; }

        public IContentObject CreateObject(int contentType)
        {
            return CreateObject((T)Enum.ToObject(typeof(T), contentType));
        }

        public abstract IContentObject CreateObject(T contentType);
    }
}
