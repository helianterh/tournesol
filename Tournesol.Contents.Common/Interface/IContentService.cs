﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Interface
{
    public interface IContentService
    {
        Guid ServiceId { get; }

        IContentObject CreateObject(int contentType);
    }
}
