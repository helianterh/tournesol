﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Interface
{
    public interface IContentObject
    {
        public bool CanHaveChildrenOf(int contentType);

        public bool CanBeChildOf(int contentType);
    }
}
