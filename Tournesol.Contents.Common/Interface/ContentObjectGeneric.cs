﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Contents.Common.Interface
{
    public abstract class ContentObject<T> : IContentObject where T : Enum
    {
        public bool CanHaveChildrenOf(int contentType)
        {
            return CanHaveChildrenOf((T)Enum.ToObject(typeof(T), contentType));
        }

        public bool CanBeChildOf(int contentType)
        {
            return CanBeChildOf((T)Enum.ToObject(typeof(T), contentType));
        }

        public abstract bool CanHaveChildrenOf(T contentType);

        public abstract bool CanBeChildOf(T contentType);
    }
}
