﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.Common.Model;

namespace Tournesol.Contents.Common.Interface
{
    public interface IContentFacadePersistence
    {
        void UpdateContent(Content content);

        Content? GetContent(Guid content);

        IEnumerable<Content> GetContents(Guid serviceId, int contentType);
    }
}
