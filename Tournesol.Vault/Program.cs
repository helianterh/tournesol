using Microsoft.EntityFrameworkCore;
using Tournesol.Contents.Persistence;
using Tournesol.Extension;
using Tournesol.Vault.Common;
using Tournesol.Vault.Common.Model;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped(provider =>
    new SecretRepository(
        new SecretFacadePersistanceEFCore(b =>
        {
            b.UseSqlite("DataSource=Secret.db");
        },
        (c, firstTime) =>
        {
            if (firstTime)
            {
                /*
                c.Database.EnsureDeleted();
                c.Database.EnsureCreated();
                c.Add(new Secret(ServicesGuid.BLOG, Guid.NewGuid().ToString().ToUpper()));
                c.SaveChanges();
                */
            }
        })
    )
);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
