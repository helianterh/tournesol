﻿using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using Tournesol.Vault.Common;

namespace Tournesol.Vault.Controllers
{
    /// <summary>
    /// Commands for requesting secret by services.
    /// Theses commands does NOT require JWT validation.
    /// Only the Authentification or Contents services need to be connect to this controller.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class VaultController
    {
        private SecretRepository _secretRepository;

        public VaultController(SecretRepository secretRepository)
        {
            _secretRepository = secretRepository;
        }


        [Route("/api/v1/GetSecret")]
        [HttpGet]
        public string GetSecret(Guid serviceId)
        {
            return _secretRepository.GetSecret(serviceId);
        }
    }
}
