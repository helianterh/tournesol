using Microsoft.EntityFrameworkCore;
using Tournesol.Contents.Common.Interface;
using Tournesol.Contents.Common.Model;
using Tournesol.Contents.Persistence;
using Tournesol.Extension;

namespace Tournesol.Contents.Tests
{
    [TestClass]
    public class Persistence_SQLite : Persistence
    {
        [TestMethod]
        public override void AddBlog()
        {
            base.AddBlog();
        }

        [TestMethod]
        public override void DeleteBlog()
        {
            base.DeleteBlog();
        }

        [TestMethod]
        public override void EditBlog()
        {
            base.EditBlog();
        }

        [TestMethod]
        public override void PublishBlog()
        {
            base.PublishBlog();
        }

        [TestMethod]
        public override void AddBlogPost()
        {
            base.AddBlogPost();
        }

        [TestMethod]
        public override void DeletePost()
        {
            base.DeletePost();
        }

        [TestMethod]
        public override void EditPost()
        {
            base.EditPost();
        }

        [TestMethod]
        public override void PublishPost()
        {
            base.PublishPost();
        }

        [TestMethod]
        public override void AddComment()
        {
            base.AddComment();
        }

        [TestMethod]
        public override void DeleteComment()
        {
            base.DeleteComment();
        }

        [TestMethod]
        public override void EditComment()
        {
            base.EditComment();
        }

        protected override IContentFacadePersistence ObtenirFacadePersistance()
        {
            var repository = new ContentFacadePersistanceEFCore(
            options => options.UseSqlite("Data Source=Content.db"),
            (context, firstTime) =>
            {
                if (firstTime)
                {
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                }
            });
            return repository;
        }
    }
}