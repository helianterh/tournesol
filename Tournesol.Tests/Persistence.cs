﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.BlogService;
using Tournesol.Contents.BlogService.Model;
using Tournesol.Contents.Common;
using Tournesol.Contents.Common.Interface;
using Tournesol.Contents.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Contents.Tests
{
    public abstract class Persistence
    {
        public virtual void AddBlog()
        {
            this._AddBlog();
        }

        public virtual void DeleteBlog()
        {
            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());

            var blog = _AddBlog();
            var blogPersistent_1 = repository.GetContent(blog.ContentId.Value);
            Assert.IsNotNull(blogPersistent_1);

            Assert.IsTrue(blogPersistent_1.State == VersionState.Added);
            repository.DeleteContent(blogPersistent_1);
            Assert.IsTrue(blogPersistent_1.State == VersionState.Deleted);

            var blogPersistent_2 = repository.GetContent(blog.ContentId.Value);
            Assert.IsNotNull(blogPersistent_2);

            Assert.AreNotEqual(blog.ToJson(), blogPersistent_2.ToJson());
            Assert.AreEqual(blogPersistent_1.ToJson(), blogPersistent_2.ToJson());
        }

        public virtual void EditBlog()
        {
            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());
            var blogContent = this._AddBlog();

            Assert.IsNotNull(blogContent.ContentId);
            Assert.IsTrue(blogContent.State == VersionState.Added);

            var blogPersistent1 = repository.GetContent(blogContent.ContentId.Value);
            Assert.IsNotNull(blogPersistent1);
            Assert.AreEqual(blogPersistent1.ToJson(), blogContent.ToJson());

            var blogObject = blogContent.GetContentObject<Blog>();
            blogObject.Title = "Title : EditBlog v2.0";
            blogObject.Description = "Description : EditBlog v2.0";
            repository.UpdateContent(blogContent, blogObject);

            var blogPersistent2 = repository.GetContent(blogContent.ContentId.Value);
            Assert.IsNotNull(blogPersistent2);
            Assert.AreEqual(blogPersistent2.ToJson(), blogContent.ToJson());
        }

        public virtual void PublishBlog()
        {
            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());
            var blogContent = _AddBlog();

            Assert.IsNotNull(blogContent.ContentId);
            Assert.IsTrue(blogContent.State == VersionState.Added);

            repository.PublishContent(blogContent);
            var blogPersistent = repository.GetContent(blogContent.ContentId.Value);

            Assert.IsNotNull(blogPersistent);
            Assert.IsTrue(blogPersistent.State == VersionState.Published);
            Assert.AreEqual(blogPersistent.ToJson(), blogContent.ToJson());
        }

        public virtual void AddBlogPost()
        {
            this._AddBlogPost();
        }

        public virtual void DeletePost()
        {
            var blogPostContent = this._AddBlogPost();
            Assert.IsNotNull(blogPostContent.ContentId);

            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());

            var postPersistent_1 = repository.GetContent(blogPostContent.ContentId.Value);
            Assert.IsNotNull(postPersistent_1);
            Assert.IsNotNull(postPersistent_1.ContentId);

            Assert.IsTrue(postPersistent_1.State == VersionState.Added);
            repository.DeleteContent(postPersistent_1);
            Assert.IsTrue(postPersistent_1.State == VersionState.Deleted);

            var postPersistent_2 = repository.GetContent(blogPostContent.ContentId.Value);
            Assert.IsNotNull(postPersistent_2);

            Assert.AreNotEqual(blogPostContent.ToJson(), postPersistent_2.ToJson());
            Assert.AreEqual(postPersistent_1.ToJson(), postPersistent_2.ToJson());
        }

        public virtual void EditPost()
        {
            var postContent = this._AddBlogPost();
            Assert.IsNotNull(postContent.ContentId);

            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());

            Assert.IsNotNull(postContent.ContentId);
            Assert.IsTrue(postContent.State == VersionState.Added);

            var postPersistent1 = repository.GetContent(postContent.ContentId.Value);
            Assert.IsNotNull(postPersistent1);
            Assert.AreEqual(postPersistent1.ToJson(), postContent.ToJson());

            var postObject = postContent.GetContentObject<BlogPost>();

            postObject.Title = "Title : EditPost v2.0";
            postObject.Text = "ContentHTML : EditPost v2.0";
            repository.UpdateContent(postContent, postObject);

            var postPersistent2 = repository.GetContent(postContent.ContentId.Value);
            Assert.IsNotNull(postPersistent2);
            Assert.AreEqual(postPersistent2.ToJson(), postContent.ToJson());
        }

        public virtual void PublishPost()
        {
            var postContent = _AddBlogPost();
            Assert.IsNotNull(postContent.ContentId);

            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());
            
            Assert.IsTrue(postContent.State == VersionState.Added);

            repository.PublishContent(postContent);
            var postPersistent = repository.GetContent(postContent.ContentId.Value);

            Assert.IsNotNull(postPersistent);
            Assert.IsTrue(postPersistent.State == VersionState.Published);
            Assert.AreEqual(postPersistent.ToJson(), postContent.ToJson());
        }

        public virtual void AddComment()
        {
            this._AddComment();
        }

        public virtual void DeleteComment()
        {
            var comment = this._AddComment();
            Assert.IsNotNull(comment.ContentId);

            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());

            var commentPersistent_1 = repository.GetContent(comment.ContentId.Value);
            Assert.IsNotNull(commentPersistent_1);

            Assert.IsTrue(commentPersistent_1.State == VersionState.Added);
            repository.DeleteContent(commentPersistent_1);
            Assert.IsTrue(commentPersistent_1.State == VersionState.Deleted);

            var commentPersistent_2 = repository.GetContent(comment.ContentId.Value);
            Assert.IsNotNull(commentPersistent_2);

            Assert.AreNotEqual(comment.ToJson(), commentPersistent_2.ToJson());
            Assert.AreEqual(commentPersistent_1.ToJson(), commentPersistent_2.ToJson());
        }

        public virtual void EditComment()
        {
            var commentContent = this._AddComment();
            Assert.IsNotNull(commentContent.ContentId);

            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());

            Assert.IsTrue(commentContent.State == VersionState.Added);

            var commentPersistent1 = repository.GetContent(commentContent.ContentId.Value);
            Assert.IsNotNull(commentPersistent1);
            Assert.AreEqual(commentPersistent1.ToJson(), commentContent.ToJson());

            var comment = commentContent.GetContentObject<Comment>();
            comment.Text = "Text : EditComment v2.0";
            repository.UpdateContent(commentContent, comment);

            var commentPersistent2 = repository.GetContent(commentContent.ContentId.Value);
            Assert.IsNotNull(commentPersistent2);
            Assert.AreEqual(commentPersistent2.ToJson(), commentContent.ToJson());
        }

        protected abstract IContentFacadePersistence ObtenirFacadePersistance();

        private Content _AddBlog()
        {
            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());

            var blog = repository.AddContent(new BlogContentService(), BlogContentType.Blog.Id(), null, new Blog() { Title = "Title : AddBlog", Description = "Description : AddBlog" });
            Assert.IsNotNull(blog.ContentId);

            var blogPersistent = repository.GetContent(blog.ContentId.Value);

            Assert.IsNotNull(blogPersistent);
            Assert.AreEqual(blogPersistent.ToJson(), blog.ToJson());

            return blog;
        }

        private Content _AddBlogPost()
        {
            var blog = this._AddBlog();
            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());
            var blogPost = repository.AddContent(new BlogContentService(), BlogContentType.BlogPost.Id(), blog.ContentId,
                new BlogPost() { Title = "Title : AddBlogPost", Text = "Description : AddBlogPost" });

            Assert.IsNotNull(blogPost.ContentId);
            var blogPostPersistent = repository.GetContent(blogPost.ContentId.Value);

            Assert.IsNotNull(blogPostPersistent);
            Assert.AreEqual(blogPostPersistent.ToJson(), blogPost.ToJson());

            return blogPost;
        }

        private Content _AddComment()
        {
            var blogPost = this._AddBlogPost();
            var repository = new ContentRepository(ObtenirFacadePersistance(), ObtenirFacadeUser());
            var comment = repository.AddContent(new BlogContentService(), BlogContentType.Comment.Id(), blogPost.ContentId, 
                new Comment() { Text = "Text : AddComment" });
            Assert.IsNotNull(comment.ContentId);

            var commentPersistent = repository.GetContent(comment.ContentId.Value);

            Assert.IsNotNull(commentPersistent);
            Assert.AreEqual(commentPersistent.ToJson(), comment.ToJson());

            return comment;
        }

        private class FacadeUser : IFacadeUser
        {
            private static Guid _userId = Guid.NewGuid();

            public Guid GetCurrentUserGuid()
            {
                return _userId;
            }
        }

        protected IFacadeUser ObtenirFacadeUser()
        {
            return new FacadeUser();
        }
    }
}
