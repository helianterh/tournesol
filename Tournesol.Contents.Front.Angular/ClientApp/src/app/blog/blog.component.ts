import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  _http: HttpClient;
  baseURL: string = "https://localhost:7221/";

  constructor(http: HttpClient) {

    this._http = http;
  }

  ngOnInit(): void {
  }

  addBlog(): void {

    var json = JSON.stringify({
      "Title": "Blog de Angular",
      "Description": "Description batard"
    });

    var body = JSON.stringify({
      "contentTypeId": 0,
      "parentId": null,
      "json": json
    });

    const headers = { 'content-type': 'application/json', 'accept': 'text / plain' };

    this._http.post(this.baseURL + 'api/v1/addContent', body, { 'headers': headers }).subscribe(result => {

    }, error => console.error(error));
  }

}

interface Blog {

}
