﻿using Microsoft.EntityFrameworkCore;
using Tournesol.Authentification.Common.Model;
using Tournesol.Authentification.Persistence.Configuration;

namespace Tournesol.Authentification.Persistence
{
    public class UserDbContext : DbContext
    {
        public DbSet<User> User { get; set; }

        public UserDbContext(Action<DbContextOptionsBuilder> actionBuilder) : this(Init(actionBuilder))
        {

        }

        public UserDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        private static DbContextOptions Init(Action<DbContextOptionsBuilder> actionBuilder)
        {
            var builder = new DbContextOptionsBuilder();
            actionBuilder!(builder);
            return builder.Options;
        }
    }
}