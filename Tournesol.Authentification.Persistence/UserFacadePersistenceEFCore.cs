﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Authentification.Common.Interface;
using Tournesol.Authentification.Common.Model;
using Tournesol.Authentification.Persistence;

namespace Tournesol.Contents.Persistence
{
    public class UserFacadePersistenceEFCore : IUserFacadePersistence
    {
        private static bool _firstTime = true;
        private static object _firstTimeLock = new object();

        private Action<DbContextOptionsBuilder> _actionBuilder;
        private Action<DbContext, bool> _actionContext;

        public UserFacadePersistenceEFCore(Action<DbContextOptionsBuilder> actionBuilder, Action<DbContext, bool> actionContext)
        {
            _actionBuilder = actionBuilder;
            _actionContext = actionContext;
        }

        public void UpdateUser(User user)
        {
            using (var context = GetContext())
            {
                context.User.Update(user);
                context.SaveChanges();
            }
        }

        public User? GetUserByEmail(Guid serviceId, string email)
        {
            using (var context = GetContext())
            {
                return context.User.FirstOrDefault(u => u.ServiceId == serviceId && u.Email == email);
            }
        }

        private UserDbContext GetContext()
        {
            var context = new UserDbContext(_actionBuilder);

            lock (_firstTimeLock)
            {
                _actionContext!(context, _firstTime);
                _firstTime = false;
            }

            return context;
        }


    }
}
