﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Authentification.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Authentification.Persistence.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("USER");
            builder.HasKey(e => e.Id);

            int order = 0;
            builder.Property(e => e.Id)
                .HasColumnName("USER_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.ServiceId)
                .HasColumnName("SERVICE_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.Email)
                .HasColumnName("USER_EMAIL")
                .HasColumnOrder(order++)
                .HasMaxLength(320)
                .IsRequired();

            builder.HasIndex(e => new { e.ServiceId, e.Email })
                .IsUnique();

            builder.Property(e => e.Password)
                .HasColumnName("USER_PASSWORD")
                .HasColumnOrder(order++)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(e => e.State)
                .HasColumnName("USER_STATE")
                .HasColumnOrder(order++)
                .HasColumnType("varchar")
                .HasMaxLength(3)
                .HasConversion<string>(app_val => app_val.KeyValue(), bd_val => Enumerations.Get<UserState>(bd_val))
                .IsRequired();

            builder.Property(e => e.CreatedDate)
                .HasColumnName("USER_CREATED_DATE")
                .HasColumnOrder(order++)
                .HasColumnType("date")
                .IsRequired()
                .HasConversion(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));

            builder.Property(e => e.Secret)
                .HasColumnName("SECRET")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.SecretCreatedDate)
                .HasColumnName("SECRET_CREATED_DATE")
                .HasColumnOrder(order++)
                .HasColumnType("date")
                .IsRequired()
                .HasConversion(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));
        }
    }
}
