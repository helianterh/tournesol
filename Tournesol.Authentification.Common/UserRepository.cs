﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Authentification.Common.Interface;
using Tournesol.Authentification.Common.Model;

namespace Tournesol.Authentification.Common
{
    public class UserRepository
    {
        private IUserFacadePersistence _facadePersistance;

        public UserRepository(IUserFacadePersistence facadePersistance)
        {
            _facadePersistance = facadePersistance;
        }

        public User? GetUserByEmail(Guid serviceId, string email)
        {
            return _facadePersistance.GetUserByEmail(serviceId, email);
        }

        public void UpdateUser(User user)
        {
            _facadePersistance.UpdateUser(user);
        }
    }
}
