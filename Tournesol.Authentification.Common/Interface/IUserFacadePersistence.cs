﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Authentification.Common.Model;

namespace Tournesol.Authentification.Common.Interface
{
    public interface IUserFacadePersistence
    {
        User? GetUserByEmail(Guid serviceId, string email);

        void UpdateUser(User user);
    }
}
