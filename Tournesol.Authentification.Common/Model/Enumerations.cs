﻿using Tournesol.Extension;
using static Tournesol.Attributes.Attributes;

namespace Tournesol.Authentification.Common.Model
{
    public enum UserState
    {
        [KeyValue("PEN")]
        Pending = 0,

        [KeyValue("VAL")]
        Validated = 1,

        [KeyValue("DEA")]
        Deactived = 2,

        [KeyValue("BAN")]
        Banned = 3,
    }

    public static class Enumerations
    {
        private static Dictionary<Tuple<Type, string>, Enum> _cache = InitCache();

        public static T Get<T>(string keyValue) where T : Enum
        {
            var key = Tuple.Create(typeof(T), keyValue);
            if (_cache.ContainsKey(key))
                return (T)_cache[key];

            throw new TournesolException("KeyValue is not corresponding to an existing Enum.");
        }

        private static Dictionary<Tuple<Type, string>, Enum> InitCache()
        {
            var dictio = new Dictionary<Tuple<Type, string>, Enum>();

            Action<Type> addCache = type =>
            {
                foreach (Enum enumeration in Enum.GetValues(type))
                    dictio.Add(Tuple.Create(type, enumeration.KeyValue()), enumeration);
            };

            addCache(typeof(UserState));
            return dictio;
        }
    }
}
