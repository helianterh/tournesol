﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Extension;

namespace Tournesol.Authentification.Common.Model
{
    public class User
    {
        public Guid Id { get; set; }

        public Guid ServiceId { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public UserState State { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid Secret { get; set; }

        public DateTime SecretCreatedDate { get; set; }

        public User(Guid id, Guid serviceId, string email, string password, UserState state, DateTime createdDate, Guid secret, DateTime secretCreatedDate)
        {
            ServiceId = serviceId;
            Id = id;
            Email = email;
            Password = password;
            State = state;
            CreatedDate = createdDate;
            Secret = secret;
            SecretCreatedDate = secretCreatedDate;
        }

        public void ValidateSecret(Guid secret)
        {
            //Validate if the secret is the same
            if (!Equals(this.Secret, secret))
                throw new TournesolException("The sended secret is not the same has the user secret.");

            //Validate if the secret generated date is expired
            if (this.SecretCreatedDate < DateTime.UtcNow.AddMonths(-1))
                throw new TournesolException("The user secret is expired.");
        }

        public void ValidatePassword(string password)
        {
            //Validate if the secret is the same
            if (!Equals(this.Password, password))
                throw new TournesolException("The sended password is not the same has the user password.");
        }

        public void ValidateState(UserState state)
        {
            if (!Equals(this.State, state))
                throw new TournesolException($"The user is not in {state.KeyValue} state.");
        }
    }
}
