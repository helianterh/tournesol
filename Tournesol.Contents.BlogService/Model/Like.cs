﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.BlogService.Model
{
    public class Like : ContentObject<BlogContentType>
    {
        public bool IsLike { get; set; }

        public override bool CanBeChildOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.Blog ||
                   contentType == BlogContentType.BlogPost ||
                   contentType == BlogContentType.Comment;
        }

        public override bool CanHaveChildrenOf(BlogContentType contentType) => false;
    }
}
