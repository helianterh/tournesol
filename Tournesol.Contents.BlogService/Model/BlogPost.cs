﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.BlogService.Model
{
    public class BlogPost : ContentObject<BlogContentType>
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public override bool CanBeChildOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.Blog;
        }

        public override bool CanHaveChildrenOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.Like ||
                contentType == BlogContentType.Comment;
        }
    }
}