﻿using Tournesol.Extension;
using static Tournesol.Attributes.Attributes;

namespace Tournesol.Contents.BlogService.Model
{
    public enum BlogContentType
    {
        [KeyValue("NON")]
        None = -1,

        [KeyValue("BLO")]
        Blog = 0,

        [KeyValue("POS")]
        BlogPost = 1,

        [KeyValue("COM")]
        Comment = 2,

        [KeyValue("LIK")]
        Like = 3,

        [KeyValue("DIA")]
        Diary = 4,

        [KeyValue("ENT")]
        DiaryEntry = 5,
    }

    public static class Enumerations
    {
        private static Dictionary<Tuple<Type, string>, Enum> _cache = InitCache();

        public static T Get<T>(string keyValue) where T : Enum
        {
            var key = Tuple.Create(typeof(T), keyValue);
            if (_cache.ContainsKey(key))
                return (T)_cache[key];

            throw new TournesolException("KeyValue is not corresponding to an existing Enum.");
        }

        private static Dictionary<Tuple<Type, string>, Enum> InitCache()
        {
            var dictio = new Dictionary<Tuple<Type, string>, Enum>();

            Action<Type> addCache = type =>
            {
                foreach (Enum enumeration in Enum.GetValues(type))
                    dictio.Add(Tuple.Create(type, enumeration.KeyValue()), enumeration);
            };

            addCache(typeof(BlogContentType));
            return dictio;
        }
    }
}
