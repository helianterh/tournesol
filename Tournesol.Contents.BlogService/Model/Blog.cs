﻿using System.ComponentModel.DataAnnotations.Schema;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.BlogService.Model
{
    public class Blog : ContentObject<BlogContentType>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public override bool CanBeChildOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.None;
        }

        public override bool CanHaveChildrenOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.BlogPost ||
                   contentType == BlogContentType.Like ||
                   contentType == BlogContentType.Comment;
        }
    }
}