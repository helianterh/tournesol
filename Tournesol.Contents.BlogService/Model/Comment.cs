﻿using System.ComponentModel.DataAnnotations.Schema;
using Tournesol.Contents.Common.Interface;

namespace Tournesol.Contents.BlogService.Model
{
    public class Comment : ContentObject<BlogContentType>
    {
        public string Text { get; set; }


        public override bool CanBeChildOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.Blog ||
                contentType == BlogContentType.BlogPost ||
                contentType == BlogContentType.Comment;
        }

        public override bool CanHaveChildrenOf(BlogContentType contentType)
        {
            return contentType == BlogContentType.Like ||
                contentType == BlogContentType.Comment;
        }
    }
}