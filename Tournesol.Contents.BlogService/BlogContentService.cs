﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.BlogService.Model;
using Tournesol.Contents.Common.Interface;
using Tournesol.Extension;

namespace Tournesol.Contents.BlogService
{
    public class BlogContentService : ContentService<BlogContentType>
    {
        public override Guid ServiceId => Services.BLOG.ServiceId;

        public override IContentObject CreateObject(BlogContentType contentType)
        {
            switch (contentType)
            {
                case BlogContentType.None: return null;
                case BlogContentType.Blog: return new Model.Blog();
                case BlogContentType.BlogPost: return new BlogPost();
                case BlogContentType.Comment: return new Comment();
                case BlogContentType.Like: return new Like();
                case BlogContentType.Diary: return null;
                case BlogContentType.DiaryEntry: return null;
            }
            return null;
        }
    }
}
