﻿using Tournesol.Vault.Vault.Interface;

namespace Tournesol.Vault.Common
{
    public class SecretRepository
    {
        private ISecretFacadePersistence _facadePersistence;

        public SecretRepository(ISecretFacadePersistence facadePersistence)
        {
            _facadePersistence = facadePersistence;
        }

        public string GetSecret(Guid serviceId)
        {
            return _facadePersistence.GetSecret(serviceId);
        }
    }
}