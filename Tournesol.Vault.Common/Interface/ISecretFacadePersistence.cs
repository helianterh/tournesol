﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Vault.Vault.Interface
{
    public interface ISecretFacadePersistence
    {
        string GetSecret(Guid serviceId);
    }
}
