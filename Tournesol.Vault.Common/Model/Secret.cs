﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tournesol.Vault.Common.Model
{
    public class Secret
    {
        public Guid ServiceId { get; set; }

        public string Value { get; set; }

        public Secret(Guid serviceId, string value)
        {
            ServiceId = serviceId;
            Value = value;
        }
    }
}
