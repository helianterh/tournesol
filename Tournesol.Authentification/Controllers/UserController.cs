﻿using Microsoft.AspNetCore.Mvc;

namespace Tournesol.Authentification.Controllers
{
    /// <summary>
    /// Commands for managing existing user. Theses commands does require JWT validation.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {

        [Route("/api/v1/ResetPasswordByEmail")]
        [HttpGet]
        public void ResetPasswordByEmail(string serviceName, string userEmail)
        {
            //Check if the user exists for the specific service

            //Check if the user is in VALIDADED state

            //Change the secret for the user with a current generated date

            //Send an email to the user with the service, email and new secret for "ChangePassword" in the URL
        }

        [Route("/api/v1/ChangePassword")]
        [HttpGet]
        public void ChangePassword(string serviceName, string userEmail, string userPassword, string secret)
        {
            //Check if the user exists for the specific service

            //Check if the user is in VALIDADED state

            //Validate if the secret is valid 

            //Validate if the secret generated date is within acceptable time

            //Change the password
        }

        [Route("/api/v1/DeactiveUser")]
        [HttpGet]
        public void DeactiveUser(string serviceName, string userEmail)
        {
            //Check if the user exists for the specific service

            //Check if the user is in VALIDADED state

            //Change the user state by DEACTIVATED
        }

        [Route("/api/v1/ReactivateUserByEmail")]
        [HttpGet]
        public void ReactivateUserByEmail(string serviceName, string userEmail, string userPassword)
        {
            //Check if the user exists for the specific service

            //Check if the user is in DEACTIVATED state

            //Change the secret for the user with a current generated date

            //Send an email to the user with the service, email and new secret by "ReactivateUser" command in the URL
        }

        [Route("/api/v1/ReactivateUser")]
        [HttpGet]
        public void ReactivateUser(string serviceName, string userEmail, string secret)
        {
            //Check if the user exists for the specific service

            //Check if the user is in DEACTIVATED state

            //Change the secret for the user with a current generated date

            //Validate if the secret is valid 

            //Validate if the secret generated date is within acceptable time

            //Change the user state for "VALIDATED"
        }
    }
}
