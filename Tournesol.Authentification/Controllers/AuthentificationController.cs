﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using Tournesol.Authentification.Common;
using Tournesol.Authentification.Common.Model;
using Tournesol.Extension;
using Tournesol.Vault.Client;

namespace Tournesol.Authentification.Controllers
{
    /// <summary>
    /// Commands for creating a new user. Theses commands does NOT require JWT validation.
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class AuthentificationController : ControllerBase
    {
        public static string EMAIL_FROM = "noreply@tournesol.com";

        public UserRepository _userRepository;

        public AuthentificationController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [Route("/api/v1/AddUserWithEmail")]
        [HttpGet]
        public void RegisterNewUserByEmail(Guid serviceId, string userEmail, string userPassword)
        {
            //Check if an user already have this email for the specific service
            var user = _userRepository.GetUserByEmail(serviceId, userEmail);
            if (user != null)
                throw new TournesolException("There is already a user with the same email for the same service.");

            user = new User(Guid.NewGuid(), serviceId, userEmail, userPassword, UserState.Pending, DateTime.UtcNow, Guid.NewGuid(), DateTime.UtcNow);
            _userRepository.UpdateUser(user);

            //Send an email to the user with the service, email and secret for "ValidatePendingUser" command in the URL
            string url = "";
            var smtp = new SmtpClient();
            smtp.Send(EMAIL_FROM, userEmail, "New User", url);
        }

        [Route("/api/v1/ValidatePendingUser")]
        [HttpGet]
        public void ValidatePendingUser(Guid serviceId, string userEmail, Guid secret)
        {
            //Check if the user exists for the specific service
            var user = _userRepository.GetUserByEmail(serviceId, userEmail);
            if(user == null)
                throw new TournesolException($"There is no user with the corresponding email {userEmail} for the service {serviceId}.");

            //Check if the user is in PENDING state
            user.ValidateState(UserState.Pending);

            //Validate if the secret is valid 
            user.ValidateSecret(secret);

            //Change the user state for VALIDADED 
            user.State = UserState.Validated;
            _userRepository.UpdateUser(user);
        }


        [Route("/api/v1/CreateToken")]
        [HttpGet]
        public string CreateToken(Guid serviceId, string userEmail, string userPassword)
        {
            //Check if the user exists for the specific service
            var user = _userRepository.GetUserByEmail(serviceId, userEmail);
            if (user == null)
                throw new TournesolException($"There is no user with the corresponding email {userEmail} for the service {serviceId}.");

            //Check if the user is VALIDADED
            user.ValidateState(UserState.Validated);

            //Check if the password is valid
            user.ValidatePassword(userPassword);

            //Create JWT Token with the secret link to the service
            return _CreateToken(user);
        }

        private string _CreateToken(User user)
        {
            var service = Services.GetService(user.ServiceId);
            var secret = VaultClient.GetSecretAsync(user.ServiceId).GetAwaiter().GetResult();

            var key = Encoding.UTF8.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("Id", Guid.NewGuid().ToString()),
                    new Claim(Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                    new Claim(Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames.Email, user.Email),
                    new Claim("ServiceId", user.ServiceId.ToString()),
                    new Claim("ServiceName", service.Name),
                    new Claim("UserState", user.State.KeyValue()),
                }),
                Expires = DateTime.UtcNow.AddMinutes(5),
                Issuer = service.ValidIssuer,
                Audience = service.ValidAudience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }




    }
}
