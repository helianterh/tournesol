﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Authentification.Persistence;
using Tournesol.Vault.Vault.Interface;

namespace Tournesol.Contents.Persistence
{
    public class SecretFacadePersistanceEFCore : ISecretFacadePersistence
    {
        private static bool _firstTime = true;
        private static object _firstTimeLock = new object();

        private Action<DbContextOptionsBuilder> _actionBuilder;
        private Action<DbContext, bool> _actionContext;

        public SecretFacadePersistanceEFCore(Action<DbContextOptionsBuilder> actionBuilder, Action<DbContext, bool> actionContext)
        {
            _actionBuilder = actionBuilder;
            _actionContext = actionContext;
        }

        public string GetSecret(Guid serviceId)
        {
            using (var context = GetContext())
            {
                var secret = context.Secret.FirstOrDefault(s => s.ServiceId == serviceId);
                return secret == null ? String.Empty : secret.Value;
            }
        }

        private SecretDbContext GetContext()
        {
            var context = new SecretDbContext(_actionBuilder);

            lock (_firstTimeLock)
            {
                _actionContext!(context, _firstTime);
                _firstTime = false;
            }

            return context;
        }


    }
}
