﻿using Microsoft.EntityFrameworkCore;
using Tournesol.Authentification.Persistence.Configuration;
using Tournesol.Vault.Common.Model;

namespace Tournesol.Authentification.Persistence
{
    public class SecretDbContext : DbContext
    {
        public DbSet<Secret> Secret { get; set; }

        public SecretDbContext(Action<DbContextOptionsBuilder> actionBuilder) : this(Init(actionBuilder))
        {

        }

        public SecretDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new SecretConfiguration());
            
        }

        private static DbContextOptions Init(Action<DbContextOptionsBuilder> actionBuilder)
        {
            var builder = new DbContextOptionsBuilder();
            actionBuilder!(builder);
            return builder.Options;
        }
    }
}