﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Vault.Common.Model;

namespace Tournesol.Authentification.Persistence.Configuration
{
    public class SecretConfiguration : IEntityTypeConfiguration<Secret>
    {
        public void Configure(EntityTypeBuilder<Secret> builder)
        {
            builder.ToTable("SECRET");
            builder.HasKey(e => e.ServiceId);

            int order = 0;
            builder.Property(e => e.ServiceId)
                .HasColumnName("SERVICE_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.Value)
                .HasColumnName("VALUE")
                .HasColumnOrder(order++)
                .IsRequired();
        }
    }
}
