﻿using Microsoft.EntityFrameworkCore;
using Tournesol.Contents.Common.Model;
using Tournesol.Contents.Persistence.Configuration;

namespace Tournesol.Contents.Persistence
{
    internal class ContentDbContext : DbContext
    {
        public DbSet<Content> Content { get; set; }

        public DbSet<ContentVersion> ContentVersion { get; set; }

        public ContentDbContext(Action<DbContextOptionsBuilder> actionBuilder) : this(Init(actionBuilder))
        {

        }

        public ContentDbContext(DbContextOptions options) : base(options)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new ContentConfiguration());
            modelBuilder.ApplyConfiguration(new ContentVersionConfiguration());
        }

        private static DbContextOptions Init(Action<DbContextOptionsBuilder> actionBuilder)
        {
            var builder = new DbContextOptionsBuilder();
            actionBuilder!(builder);
            return builder.Options;
        }
    }
}