﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Contents.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Contents.Persistence.Configuration
{
    public class ContentConfiguration : IEntityTypeConfiguration<Content>
    {
        public void Configure(EntityTypeBuilder<Content> builder)
        {
            builder.ToTable("CONTENT");
            builder.HasKey(e => e.ContentId);

            int order = 0;
            builder.Property(e => e.ContentId)
                .HasColumnName("CONTENT_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.ServiceId)
                .HasColumnName("SERVICE_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.ParentId)
                .HasColumnName("PARENT_ID")
                .HasColumnOrder(order++);

            builder.Property(e => e.ContentType)
                .HasColumnName("CONTENT_TYPE")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.Json)
                .HasColumnName("JSON")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.Api)
                .HasColumnName("API")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.State)
                .HasColumnName("VERSION_STATE")
                .HasColumnOrder(order++)
                .HasColumnType("varchar")
                .HasMaxLength(3)
                .HasConversion<string>(app_val => app_val.KeyValue(), bd_val => Enumerations.Get<VersionState>(bd_val))
                .IsRequired();

            builder.Property(e => e.UpdatedBy)
                .HasColumnName("UPDATED_BY")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.UpdatedDate)
                .HasColumnName("UPDATED_DATE")
                .HasColumnOrder(order++)
                .HasColumnType("date")
                .IsRequired()
                .HasConversion(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));

            builder.Property(e => e.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.CreatedDate)
                .HasColumnName("CREATED_DATE")
                .HasColumnOrder(order++)
                .HasColumnType("date")
                .IsRequired()
                .HasConversion(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));

            builder.HasMany(e => e.Versions)
                .WithOne(e => e.Content)
                .HasForeignKey(e => e.ContentId);

            builder.HasMany(e => e.Children)
                .WithOne(e => e.Parent)
                .HasForeignKey(e => e.ParentId);
        }
    }
}
