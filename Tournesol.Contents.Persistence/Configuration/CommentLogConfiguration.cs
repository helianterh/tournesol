﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tournesol.Blug.Common.Model;
using Tournesol.Extension;

namespace Tournesol.Blug.Persistence.Configuration
{
    public class CommentLogConfiguration : IEntityTypeConfiguration<CommentLog>
    {
        public void Configure(EntityTypeBuilder<CommentLog> builder)
        {
            int order = 0;

            builder.ToTable("COMMENT_LOG");
            builder.HasKey(e => e.LogId);

            builder.Property(e => e.LogId)
                .HasColumnName("LOG_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.LogBy)
                .HasColumnName("LOG_BY")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.LogDate)
                .HasColumnName("LOG_DATE")
                .HasColumnOrder(order++)
                .IsRequired()
                .HasConversion<DateTime>(app_val => app_val, bd_val => new DateTime(bd_val.Ticks, DateTimeKind.Utc));

            builder.Property(e => e.CommentId)
                .HasColumnName("COMMENT_ID")
                .HasColumnOrder(order++)
                .IsRequired();

            builder.Property(e => e.State)
                .HasColumnName("COMMENT_STATE")
                .HasColumnOrder(order++)
                .IsRequired() 
                .HasConversion<string>(app_val => app_val.KeyValue(), bd_val => Enumerations.Get<CommentState>(bd_val));
        }
    }
}
